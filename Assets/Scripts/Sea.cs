﻿using UnityEngine;
using System.Collections;

public class Sea : BoardSquare {

	protected override void Awake () {
		base.Awake();
		totalHits = 0;
		images[2] = Resources.Load ("Fish") as Texture2D;
	}
	
	protected override void onSquareSelected() {
		if (totalHits == 0) {
			updateEndGraphic (this);
			updateShots (this);
		}
	}
	
	protected override void updateEndGraphic(BoardSquare ship){
		//ship.renderer.material.color = new Color (1, .5f, .5f);
		ship.GetComponent<Renderer>().material.mainTexture = images[2];
	}
}