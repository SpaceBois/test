﻿using UnityEngine;
using System.Collections;

public class BoardSquare : MonoBehaviour
{

	public int Number { get; set; }

	protected int totalHits = 0;
	protected bool hit = false;
	protected Texture2D[] images = new Texture2D[3];

	protected virtual void onSquareSelected ()
	{
	}

	protected virtual void updateEndGraphic (BoardSquare ship)
	{
	}

	protected virtual void updateShots (BoardSquare ship)
	{
		if (ship.GetType () == typeof(Submarine)) { // checks that it is not an empty SEA type square
			//any code here will get called when a ship is destroyed
		}
		//can add further type checks if required
	}

	protected virtual void Awake ()
	{
		images [0] = Resources.Load ("Water") as Texture2D;
		images [1] = Resources.Load ("Boom") as Texture2D;
		images [2] = Resources.Load ("Water") as Texture2D;

		this.GetComponent<Renderer> ().material.mainTexture = images [0];
	}

	protected virtual void OnMouseDown ()
	{
		this.GetComponent<Renderer> ().material.mainTexture = images [1];
		this.GetComponent<Collider> ().enabled = false;
		hit = true;
		onSquareSelected ();
		GameObject.Find ("GameController").GetComponent<GameController> ().shotFired ();
	}
	
}
